'use strict';
~ function() {
    var easeInOut = Power2.easeInOut,
        ad = document.getElementById('mainContent'),
        points = [
        {
            "x": 152.4,
            "y": 31.8
        },
        {
            "x": 203.4,
            "y": 33.1
        },
        {
            "x": 244.3,
            "y": 74.8
        },
        {
            "x": 244.3,
            "y": 126.1
        },
        {
            "x": 244.3,
            "y": 178.2
        },
        {
            "x": 202.10000000000002,
            "y": 220.39999999999998
        },
        {
            "x": 150,
            "y": 220.39999999999998
        },
        {
            "x": 97.89999999999998,
            "y": 220.39999999999998
        },
        {
            "x": 55.7,
            "y": 178.2
        },
        {
            "x": 55.7,
            "y": 126.09999999999998
        },
        {
            "x": 55.7,
            "y": 73.99999999999997
        },
        {
            "x": 98,
            "y": 31.8
        },
        {
            "x": 150,
            "y": 31.8
        }
    ];


    window.init = function() {
        createCoins();
        var tl = new TimelineMax();

        tl.set(ad, { force3D: false })        
        tl.staggerTo('.coinClass1', 1.5, { bezier: { type: "cubic", values: points },
            cycle: { opacity: function (a) {
                    return a * 0.1
                }                    
            }, force3D: true, rotation: 0.01, ease: easeInOut, yoyo: true, repeat: 1,},0.1, 0);
             
        }

    function createCoins() {
        for (var i = 1; i <= 10; i++) {
            var coin = document.createElement('div');
            coin.id = 'redcoin' + i;

            coin.className = 'coinClass1';
            ad.appendChild(coin);
            }
        }
}();
window.onload = init();